IT INIT 
git init  inizializza un repository vuoto in locale nel percorso dove è stato lanciato e crea la directory contenenti tutte le risorse necessarie per il corretto funzionamento di git 
GIT STATUS 
git status  notifica eventuali cambiamenti/modifiche, descrive cosa sta succedendo, indicando le risorse pronte per il commit e suggerisce azioni da effettuare , segnalando la presenza di conflitti
GIT ADD
git add nomefile.txt  registra modifiche di una risorsa e la passa nella staging area prima del commit
git add -p mostra le modifiche apportate ai file uno alla volta e chiede all'utente di decidere se aggiungere o meno ogni modifica al repository. Questo consente di controllare esattamente ciò che viene aggiunto al repository e di evitare di aggiungere accidentalmente modifiche non necessarie o indesiderate.
git add .  aggiunge tutte le modifiche apportate alle risorse nella staging area (tranne quelle nei file esclusi tramite il .gitignore file)
GIT COMMIT
git commit  fornisce l’output di git status
git commit --amend  consente di aprire l’editor di testo per modificare il messaggio dell’ultimo commit effettuato. Si può anche usare per aggiungere modifiche non committate al commit precedente al posto di creare un commit nuovo 
git commit --amend --reset-author permette di ripristinare l’identità del commit
git commit -m “commento”
GIT RM 
git rm nomefile.txt -eliminare un file
git rm --cached nomefile.txt elimina il file dal repository ma la risorsa rimane memorizzata nel file system ( e si può riaggiungere)
 git mv nomevecchio.txt nomenuovo.txt  per rinominare una risorsa 
mv nomevecchio.txt nomenuovo.txt è corretto per rinominare file su linux, ma su git c’è il rischio di eliminare il file e crearne uno nuovo vuoto
GIT SHOW
git show  mostra l’ultimo commit effettuato con il suo id 
GIT LOG
git log  mostra hash univoco del commit ed il puntatore della storia. Può indicare gli id dei branch che erano stati uniti se nel progetto erano state svolte merge in precedenza
Se esegui un commit in un punto della storia diverso dal branch principale (come ad esempio creando un nuovo branch da un commit precedente), la modifica non andrà persa. Tuttavia, se non ti sposti sul nuovo branch (o non crei un nuovo branch) prima di apportare ulteriori modifiche, quelle modifiche saranno apportate al branch principale e non al nuovo branch, rendendo la modifica orfana e difficile da raggiungere.
git log --graph --pretty=oneline --decorate
git log --pretty=oneline --graph --all --decorate
GIT CHECKOUT
git checkout consente di navigare nella storia di git / porta la versione di quei file all’ultimo commit utile
git checkout *id* Git cambia il riferimento HEAD (punta alla posizione corrente nel repository) e lo imposta sulla destinazione specificata da "id". Ciò significa che il repository di lavoro passerà al commit corrispondente, al branch o al tag specificato. 
git checkout master  si sposta nel branch master 
cat HEAD   Il comando "cat HEAD" viene utilizzato nella shell per visualizzare il contenuto del file HEAD all'interno di un repository Git. Il file HEAD è un puntatore che fa riferimento al ramo corrente o al commit corrente nel repository. 
HEAD  puntatore che indica dove siamo nella storia
git checkout HEAD   utilizzato per spostarsi nel commit corrente del repository. 
git checkout HEAD^  utilizzato per spostarsi al commit padre (commit precedente) del commit corrente nel repository. 
git checkout main   permette di spostarsi sul ramo principale (main) di un repository Git. 
git checkout nomebranch  utilizzato  per spostarsi su un ramo specifico all'interno di un repository. 
git switch -c 'nome nuovo branch'  crea il nuovo branch e ci si sposta all’interno
git checkout -b 'branchdoveandare'  fa la stessa cosa del precedente
git checkout .	OCCHIO  annulla qualsiasi modifica sia stata effettuata sulle risorse non ancora committate e rimuove tutto ciò che era nella staging area?
Branch etichetta che punta ad un commit
GIT MERGE 
git merge nomesecondobranch  importa il contenuto del branch dichiarato in quello dove attualmente mi trovo
GIT GONFIG 
git config --global init.defaultBranch main  inizializza il branch di default (master diventerà main)
GIT REVERT
git revert crea un nuovo commit che annulla le modifiche introdotte da un commit precedente 
git revert *id commit*  crea un nuovo commit opposto, negativo a quello indicato con l’id
se ho un unico commit “della disperazione” con tutto all’interno mi tocca aprire il file con un editor e lavorare sulle modifiche di ogni riga necessaria
GIT REBASE 
git rebase  consente di spostare i commit da un ramo all'altro. 
SSH
ssh keygen -t ed25519 -f 'percorso'  generazione chiavi per github/gitlab
ssh -T git@gitlab.com  per verificare che le chiavi gitlab funzionino correttamente
ssh -T -i ~percorsodellachiavewse
GIT REMOTE ADD 
git remote add origin git@gitlab.com:ecc. link del progetto collega il repository locale a uno remoto 
GIT FETCH
git fetch nomrepremoto   recupera tutte le informazioni dal repository remoto .Dopo aver eseguito il comando, puoi esaminare le modifiche e decidere se integrarle nel tuo ramo di lavoro locale.
git fetch upstream main  indica da dove e cosa importare
GIT PUSH 
invia le modifiche apportate ai repository locali su un repository remoto. 
git push origin main invia i commit presenti nel ramo "main" del tuo repository locale al repository remoto chiamato "origin". 
git push -f origin *branch*  forza il push 
GIT RESET
git reset --hard HEAD~3 permette di cancellare i commit (nel comando è tornato indietro di 3 )
GIT PULL 
git pull origin main esegue un merge da remoto a locale
GIT BRANCH 
git branch -D nomedelbranch  elimina il branch
git branch  fornisce elenco dei branch presenti nel progetto
GIT CONFIG 
git config --global  mposta e visualizza le opzioni di configurazione globali di Git, che si applicano a tutti i repository su una determinata macchina. 
cat .git/config  gestire impostazioni globali
cat ../.gitconfig  gestire impostazioni dell’utente
attenzione a cambiare l'autore di commit già pubblicati, rischio di conflitti in caso di pull da altri pc
git diff main

GIT STASH 
git stash -> salva la situazione dove mi trovo attualmente e git status non notifica le modifiche da committare
git stash pop  restituisce la situazione di prima con le modifiche non committate
git stash list  elenca gli elementi all’interno
git cherry-pick *hashdelcommit* -> alternativa di git stash
GIT RESTORE 
git restore o git amend annullano le modifiche al internno dei file 
git restore --staged . rimuove tutti i file presenti nell'area di staging, 
GIT TAG 
git tag *numversione*  posso apporre etichette sui commit
GIT LOG
git log –oneline visualizza  I commit in maniera compatta 
GIT RESET 
git reset -> elimina commit, posso scegliere se mantenere le modifiche o eliminare tutto
git reset HEAD^ sposta l’head indietro di un commit (utile se ad esempio avessi svolto cherry-pick su un commit sbagliato)
git reset --hard elimina anche le modifiche
Sintassi interne per il git ignore file:
(*) -> ignora tutto
Nomefile.txt  ignora un singolo file
README.MD ignora il singolo file README
Se in un’altra cartella creo un secondo file README.MD, mi verrebbero ignorati entrambi
/README.MD ignoro solo il file README.MD della cartella corrente?
/Nomefile.txt
.*.swp
nomecartella/*.swp  ignora tutti i file .swp all’interno della cartella indicata
!nomecartella/nomefile.txt  crea un’eccezione e tiene il file in considerazione anche se la cartella che lo contiene è stata ignorata
/*.md  ignora qualsiasi file .md presente nella cartella corrente?
*.md  ignora qualsiasi file .md
!/*.md  ignora tutti i file .md presenti nelle sottocartelle tranne quello presente nella directory radice
Nomecartella ignora l’intero contenuto della cartella
**/nomecartella/** più corretto, ignora qualsiasi prefisso e postfisso che possa riguardare la cartella ignorata
.gitignore  nessuno mi impedisce di aggiungerlo, funziona comunque rimanendo nascosto
